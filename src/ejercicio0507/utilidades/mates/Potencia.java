/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio0507.utilidades.mates;

/**
 * Fichero: Potencia.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Potencia {


   public static int potencia(int a, int b) {
      if ( b==0) {
         return 1;
      }
      else {
         return a*potencia(a,b-1);
      }

   }
}

