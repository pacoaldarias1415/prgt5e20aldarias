/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e14;

/**
 * Fichero: Coche.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-dic-2013
 */
public class Camion extends Vehiculo {

  private int tara;

  Camion() {
    matricula = "C";
    tara = 10;
  }

  /**
   * @return the tara
   */
  public int getTara() {
    return tara;
  }

  /**
   * @param tara the tara to set
   */
  public void setTara(int i) {
    this.tara = tara;
  }
}
