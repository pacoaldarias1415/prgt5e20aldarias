/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0508.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Ejercicio0508 { // Pez


// Miembros
   private String nombre;

// Metodos
   public String getNombre() {
      return this.nombre;
   }

   public void setNombre(String s) {
      this.nombre=s;
   }

   public Ejercicio0508 clone() {
      Ejercicio0508 c= new Ejercicio0508();
      c. nombre = this.nombre;
      return c;
   }

   public boolean equals( Ejercicio0508 p) {
      if ( this.nombre == p.nombre ) {
         return true;
      }
      else {
         return false;
      }
   }

   public static void main ( String  [] args ) {
      Ejercicio0508 p1= new Ejercicio0508();
      Ejercicio0508 p2= new Ejercicio0508();
      Ejercicio0508 p3= new Ejercicio0508();

      p1.setNombre("Payaso");
      p2.setNombre("Espada");
      p3=p2.clone();

      System.out.println(p1.getNombre());
      System.out.println(p2.getNombre());
      System.out.println(p3.getNombre());

      if ( p2.equals(p3) ) {
         System.out.println("Iguales");
      }

   }

}
/* Ejecucion
Payaso
Espada
Espada
Iguales
*/


