/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0505.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 15-nov-2013
 */
public class Ejercicio0505 {

  public static int elemento(int f, int c) {
    if (c == 1 || f == 1) {
      return 1;
    }
    return elemento(f, c - 1) + elemento(f - 1, c);
  }

  public static void main(String[] args) {
    int numfilas = 5;
    for (int i = 1; i <= numfilas; i++) {
      for (int j = 1; j <= numfilas; j++) {
        int dato = elemento(i, j);
        System.out.format("%5d", dato, " ");
      }
      System.out.println("");
    }
  }
}
