/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0511.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Ejercicio0511 { // Primero

  static int dato=0;
  public void Ejercicio0511() {
    dato++;
  }
  public void segundo() {
    Ejercicio0511();
    this.Ejercicio0511();
  }
  public static int getdato() {
    return dato;
  }
  public static void main(String[] args) {
    Ejercicio0511 p=new Ejercicio0511();
    p.segundo();
    System.out.println(p.getdato());
  }
}
/* EJECUCION:
2
*/